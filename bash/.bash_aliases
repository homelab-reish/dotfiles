alias ll='exa --icons --group-directories-first -la'
alias ls='exa --icons --group-directories-first'

# Docker 
alias dcd='docker compose down'
alias dcu='docker compose up -d'
alias dcl='docker compose logs -f'
alias dl='docker logs'

alias mc='vim .'
alias ..='cd ..'
alias ...='cd ../..'
alias sysupdate='apt update && apt upgrade -y'