# dotfiles

Hallo zusammen!

Ich bin Philipp und leidenschaftlicher It-ler.

In diesem Repository sind meine persönlichen Konfigurationsdateien zu finden. Hier teile ich meine Einstellungen und Anpassungen für meine Systeme, mit denen ich mich richtig wohlfühle. Checkt es gerne mal aus! 🖥️🔧

> ⚠️ Hey Leute, nur 'ne kleine Info: Produkte können sich im Laufe der Zeit ändern. Ich geb mein Bestes, immer auf dem neuesten Stand zu sein, aber manchmal klappt das nicht ganz. Hoffe, das ist cool für euch! 😎

Ich hab' diese Ressourcen kostenlos für euch erstellt, damit ihr sie in euren speziellen Anwendungsfällen nutzen könnt. Falls ihr ausführliche Anleitungen zu bestimmten Tools oder Technologien sucht, schaut einfach mal auf meinem [YouTube-Kanal](https://www.youtube.com/@NetworkStudent) vorbei! 🎥🚀

## Terminal- und Anwendungs-Symbole mit Nerd-Fonts
Um Symbole in der Terminal- oder Anwendungs-Schrift anzuzeigen, setze ich auf die [Nerd Fonts](https://www.nerdfonts.com/). Im Moment benutze ich die Hack Nerd Font Mono für Terminal-Anwendungen und die Hack Nerd Font in anderen Anwendungen. 👾🖋️

---

Hello everyone!

I am Philipp and a passionate It-ler.

In this repository you can find my personal configuration files. Here I share my settings and customizations for my systems that I feel really comfortable with. Feel free to check it out! 🖥️🔧

> ⚠️ Hey guys, just a little info: products can change over time. I do my best to always be up to date, but sometimes it doesn't quite work out. Hope that's cool for you!  😎

I've created these resources for you for free so you can use them in your specific use cases. If you're looking for in-depth tutorials on specific tools or technologies, check out my [YouTube-Channel](https://www.youtube.com/@NetworkStudent)! 🎥🚀

## Terminal and Application Icons with Nerd-Fonts
To display symbols in the terminal or application font, I use the [Nerd Fonts](https://www.nerdfonts.com/). At the moment I use the Hack Nerd Font Mono for terminal applications and the Hack Nerd Font in other applications. 👾🖋️