" Setzt das Farbschema
colo slate
" Löscht Status line wenn .vimrc geladen wird
set statusline=
" Status line linke Seite
set statusline+=\ %F\ %M\ %Y\ %R
" Benutzt ein Trennzeichen um die linke von der rechten Seite zu seperieren
set statusline+=%=
" Status line rechte Seite
set statusline+=\ ascii:\ %b\ hex:\ 0x%B\ row:\ %l\ col:\ %c\ percent:\ %p%%
" Zeigt Status line auf vorletzter Zeile an
set laststatus=2
" Nummeriert die Zeilen
set number
" Aktiviert Syntax highlighting
syntax on
" Aktiviert zusätzliche Features
set nocompatible
" Netrw config
" Map <F5> to toggle on and off the hidden files
nmap <F5> gh
" Map <F6> to toggle on and off th banner
nmap <F6> I
" Turn off banner at top of the screen on startup
let g:netrw_banner = 0
" My personal preference Settings for Netrw
let g:netrw_preview = 1 " open splits to the right
let g:netrw_altv = 1
let g:netrw_liststyle = 4 " show tree listing
let g:netrw_browse_split = 3 " open files in new vertical Split
let g:netrw_winsize = 70 " set the width of page